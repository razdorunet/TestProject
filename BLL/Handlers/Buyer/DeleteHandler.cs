using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Models.Buyer.Delete;
using Data.Intarfaces;
using MediatR;
using Microsoft.Extensions.Logging;

namespace BLL.Handlers.Buyer
{
    public class DeleteHandler : IRequestHandler<DeleteRequest, DeleteResponse>
    {
        private readonly IMapper _mapper;
        private readonly IBuyerService _service;
        private readonly ILogger<DeleteHandler> _logger;

        public DeleteHandler(IMapper mapper, IBuyerService service, ILogger<DeleteHandler> logger)
        {
            _mapper = mapper;
            _service = service;
            _logger = logger;
        }

        public async Task<DeleteResponse> Handle(DeleteRequest request, CancellationToken ct)
        {
            try
            {
                _logger.LogDebug("Handler start work");
                
                var response = new DeleteResponse()
                {
                    Status = await _service.Delete(request.Id, ct)
                };
                
                _logger.LogDebug("Handler work response");
                
                return response;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                
                return new DeleteResponse()
                {
                    Status = false,
                    Error = e.Message
                };
            }
        }
    }
}