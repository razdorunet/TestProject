using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Models.Buyer.DTO;
using BLL.Models.Buyer.GetAll;
using Data.Intarfaces;
using MediatR;
using Microsoft.Extensions.Logging;

namespace BLL.Handlers.Buyer
{
    public class GetAllHandler : IRequestHandler<GetAllRequest, GetAllResponse>
    {
        private readonly IMapper _mapper;
        private readonly IBuyerService _service;
        private readonly ILogger<GetAllHandler> _logger;

        public GetAllHandler(IMapper mapper, IBuyerService service, ILogger<GetAllHandler> logger)
        {
            _mapper = mapper;
            _service = service;
            _logger = logger;
        }

        public async Task<GetAllResponse> Handle(GetAllRequest request, CancellationToken ct)
        {
            try
            {
                return new GetAllResponse()
                {
                    Data = _mapper.Map<IList<BuyerDTO>>(await _service.GetAl(ct)),
                    Status = true
                };
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                
                return new GetAllResponse()
                {
                    Data = new List<BuyerDTO>(),
                    Status = false,
                    Error = e.Message
                };
            }
        }
    }
}