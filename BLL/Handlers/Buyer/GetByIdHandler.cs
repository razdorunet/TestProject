using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Models.Buyer.DTO;
using BLL.Models.Buyer.GetById;
using Data.Intarfaces;
using MediatR;
using Microsoft.Extensions.Logging;

namespace BLL.Handlers.Buyer
{
    public class GetByIdHandler : IRequestHandler<GetByIdRequest, GetByIdResponse>
    {
        private readonly IMapper _mapper;
        private readonly IBuyerService _service;
        private readonly ILogger<GetAllHandler> _logger;

        public GetByIdHandler(IMapper mapper, IBuyerService service, ILogger<GetAllHandler> logger)
        {
            _mapper = mapper;
            _service = service;
            _logger = logger;
        }

        public async Task<GetByIdResponse> Handle(GetByIdRequest request, CancellationToken ct)
        {
            try
            {
                return new GetByIdResponse()
                {
                    Data = _mapper.Map<BuyerDTO>(await _service.GetById(request.Id, ct)),
                    Status = true,
                };
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                
                return new GetByIdResponse()
                {
                    Data = null,
                    Status = false,
                    Error = e.Message
                };
            }
        }
    }
}