using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Models.Buyer.Post;
using Data.Intarfaces;
using MediatR;
using Microsoft.Extensions.Logging;

namespace BLL.Handlers.Buyer
{
    public class PostHandler  : IRequestHandler<PostBuyerRequest, PostResponse>
    {
        private readonly IMapper _mapper;
        private readonly IBuyerService _service;
        private readonly ILogger<PostHandler> _logger;

        public PostHandler(IMapper mapper, IBuyerService service, ILogger<PostHandler> logger)
        {
            _mapper = mapper;
            _service = service;
            _logger = logger;
        }
        
        public async Task<PostResponse> Handle(PostBuyerRequest buyerRequest, CancellationToken ct)
        {
            try
            {
                var model = _mapper.Map<Data.Models.Entity.Buyer>(buyerRequest.Model);
                
                return new PostResponse()
                {
                    Id = await _service.Post(model, ct),
                    Status = true,
                };
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                
                return new PostResponse()
                {
                    Id = null,
                    Status = false,
                    Error = e.Message
                };
            }
        }
    }
}