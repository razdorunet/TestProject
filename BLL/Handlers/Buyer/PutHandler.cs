using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Models.Buyer.Put;
using Data.Intarfaces;
using MediatR;
using Microsoft.Extensions.Logging;

namespace BLL.Handlers.Buyer
{
    public class PutHandler  : IRequestHandler<PutBuyerRequest, PutResponse>
    {
        private readonly IMapper _mapper;
        private readonly IBuyerService _service;
        private readonly ILogger<PutHandler> _logger;

        public PutHandler(IMapper mapper, IBuyerService service, ILogger<PutHandler> logger)
        {
            _mapper = mapper;
            _service = service;
            _logger = logger;
        }
        
        public async Task<PutResponse> Handle(PutBuyerRequest buyerRequest, CancellationToken ct)
        {
            try
            {
                var model = _mapper.Map<Data.Models.Entity.Buyer>(buyerRequest.Model);
                
                return new PutResponse()
                {
                    Status = await _service.Put(model, ct),
                };
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                
                return new PutResponse()
                {
                    Status = false,
                    Error = e.Message
                };
            }
        }
    }
}