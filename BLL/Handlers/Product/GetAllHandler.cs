using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Models.Product.DTO;
using BLL.Models.Product.GetAll;
using Data.Intarfaces;
using MediatR;
using Microsoft.Extensions.Logging;

namespace BLL.Handlers.Product
{
    public class GetAllHandler : IRequestHandler<GetAllRequest, GetAllResponse>
    {
        private readonly IMapper _mapper;
        private readonly IProductService _service;
        private readonly ILogger<GetAllHandler> _logger;

        public GetAllHandler(IMapper mapper, IProductService service, ILogger<GetAllHandler> logger)
        {
            _mapper = mapper;
            _service = service;
            _logger = logger;
        }

        public async Task<GetAllResponse> Handle(GetAllRequest request, CancellationToken ct)
        {
            try
            {
                return new GetAllResponse()
                {
                    Data = _mapper.Map<IList<ProductDTO>>(await _service.GetAll(ct)),
                    Status = true
                };
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                
                return new GetAllResponse()
                {
                    Data = new List<ProductDTO>(),
                    Status = false,
                    Error = e.Message
                };
            }
        }
    }
}