using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Models.Product.PatchPrice;
using Data.Intarfaces;
using MediatR;
using Microsoft.Extensions.Logging;

namespace BLL.Handlers.Product
{
    public class PatchPriceHandler  : IRequestHandler<PatchPriceRequest, PatchPriceResponse>
    {
        private readonly IMapper _mapper;
        private readonly IProductService _service;
        private readonly ILogger<PatchNameHandler> _logger;

        public PatchPriceHandler(IMapper mapper, IProductService service, ILogger<PatchNameHandler> logger)
        {
            _mapper = mapper;
            _service = service;
            _logger = logger;
        }
        
        public async Task<PatchPriceResponse> Handle(PatchPriceRequest request, CancellationToken ct)
        {
            try
            {
                return new PatchPriceResponse
                {
                    Status = await _service.PatchPrice(request.Id, request.Price, ct),
                };
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                
                return new PatchPriceResponse
                {
                    Status = false,
                    Error = e.Message
                };
            }
        }
    }
}