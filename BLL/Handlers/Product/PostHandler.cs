using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Models.Product.Post;
using Data.Intarfaces;
using MediatR;
using Microsoft.Extensions.Logging;

namespace BLL.Handlers.Product
{
    public class PostHandler  : IRequestHandler<PostProductRequest, PostResponse>
    {
        private readonly IMapper _mapper;
        private readonly IProductService _service;
        private readonly ILogger<PostHandler> _logger;

        public PostHandler(IMapper mapper, IProductService service, ILogger<PostHandler> logger)
        {
            _mapper = mapper;
            _service = service;
            _logger = logger;
        }
        
        public async Task<PostResponse> Handle(PostProductRequest productRequest, CancellationToken ct)
        {
            try
            {
                var model = _mapper.Map<Data.Models.Entity.Product>(productRequest.Model);

                return new PostResponse()
                {
                    Id = await _service.Post(model, ct),
                    Status = true,
                };
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                
                return new PostResponse()
                {
                    Id = null,
                    Status = false,
                    Error = e.Message
                };
            }
        }
    }
}