using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Models.Product.Put;
using Data.Intarfaces;
using MediatR;
using Microsoft.Extensions.Logging;

namespace BLL.Handlers.Product
{
    public class PutHandler  : IRequestHandler<PutProductRequest, PutResponse>
    {
        private readonly IMapper _mapper;
        private readonly IProductService _service;
        private readonly ILogger<PutHandler> _logger;

        public PutHandler(IMapper mapper, IProductService service, ILogger<PutHandler> logger)
        {
            _mapper = mapper;
            _service = service;
            _logger = logger;
        }
        
        public async Task<PutResponse> Handle(PutProductRequest productRequest, CancellationToken ct)
        {
            try
            {
                var model = _mapper.Map<Data.Models.Entity.Product>(productRequest.Model);

                return new PutResponse()
                {
                    Status = await _service.Put(model, ct),
                };
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                
                return new PutResponse()
                {
                    Status = false,
                    Error = e.Message
                };
            }
        }
    }
}