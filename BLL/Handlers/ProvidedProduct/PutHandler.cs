using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Models.ProvidedProduct.Put;
using Data.Intarfaces;
using MediatR;
using Microsoft.Extensions.Logging;

namespace BLL.Handlers.ProvidedProduct
{
    public class PutHandler  : IRequestHandler<PutProvidedProductRequest, PutResponse>
    {
        private readonly IMapper _mapper;
        private readonly IProvidedProductService _service;
        private readonly ILogger<PutHandler> _logger;

        public PutHandler(IMapper mapper, IProvidedProductService service, ILogger<PutHandler> logger)
        {
            _mapper = mapper;
            _service = service;
            _logger = logger;
        }
        
        public async Task<PutResponse> Handle(PutProvidedProductRequest providedProductRequest, CancellationToken ct)
        {
            try
            {
                var model = _mapper.Map<Data.Models.Entity.ProvidedProduct>(providedProductRequest.Model);
                
                return new PutResponse()
                {
                    Status = await _service.Put(model, ct),
                };
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                
                return new PutResponse()
                {
                    Status = false,
                    Error = e.Message
                };
            }
        }
    }
}