using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Models.Sale.DTO;
using BLL.Models.Sale.GetAll;
using Data.Intarfaces;
using MediatR;
using Microsoft.Extensions.Logging;

namespace BLL.Handlers.Sale
{
    public class GetAllHandler : IRequestHandler<GetAllRequest, GetAllResponse>
    {
        private readonly IMapper _mapper;
        private readonly ISaleService _service;
        private readonly ILogger<GetAllHandler> _logger;

        public GetAllHandler(IMapper mapper, ISaleService service, ILogger<GetAllHandler> logger)
        {
            _mapper = mapper;
            _service = service;
            _logger = logger;
        }

        public async Task<GetAllResponse> Handle(GetAllRequest request, CancellationToken ct)
        {
            try
            {
                return new GetAllResponse()
                {
                    Data = _mapper.Map<IList<SaleDTO>>(await _service.GetAl(ct)),
                    Status = true
                };
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                
                return new GetAllResponse()
                {
                    Data = new List<SaleDTO>(),
                    Status = false,
                    Error = e.Message
                };
            }
        }
    }
}