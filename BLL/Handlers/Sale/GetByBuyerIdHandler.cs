using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Models.Sale.DTO;
using BLL.Models.Sale.GetByBuyerId;
using Data.Intarfaces;
using MediatR;
using Microsoft.Extensions.Logging;

namespace BLL.Handlers.Sale
{
    public class GetByBuyerIdHandler : IRequestHandler<GetByBuyerIdRequest, GetByBuyerIdResponse>
    {
        private readonly IMapper _mapper;
        private readonly ISaleService _service;
        private readonly ILogger<GetAllHandler> _logger;

        public GetByBuyerIdHandler(IMapper mapper, ISaleService service, ILogger<GetAllHandler> logger)
        {
            _mapper = mapper;
            _service = service;
            _logger = logger;
        }

        public async Task<GetByBuyerIdResponse> Handle(GetByBuyerIdRequest request, CancellationToken ct)
        {
            try
            {
                return new GetByBuyerIdResponse()
                {
                    Data = _mapper.Map<IList<SaleDTO>>(await _service.GetByBuyerId(request.Id, ct)),
                    Status = true,
                };
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                
                return new GetByBuyerIdResponse()
                {
                    Data = null,
                    Status = false,
                    Error = e.Message
                };
            }
        }
    }
}