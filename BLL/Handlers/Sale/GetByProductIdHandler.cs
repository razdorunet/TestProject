using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Models.Sale.DTO;
using BLL.Models.Sale.GetByProductId;
using Data.Intarfaces;
using MediatR;
using Microsoft.Extensions.Logging;

namespace BLL.Handlers.Sale
{
    public class GetByProductIdHandler : IRequestHandler<GetByProductIdRequest, GetByProductIdResponse>
    {
        private readonly IMapper _mapper;
        private readonly ISaleService _service;
        private readonly ILogger<GetAllHandler> _logger;

        public GetByProductIdHandler(IMapper mapper, ISaleService service, ILogger<GetAllHandler> logger)
        {
            _mapper = mapper;
            _service = service;
            _logger = logger;
        }

        public async Task<GetByProductIdResponse> Handle(GetByProductIdRequest request, CancellationToken ct)
        {
            try
            {
                return new GetByProductIdResponse()
                {
                    Data = _mapper.Map<IList<SaleDTO>>(await _service.GetByProductId(request.Id, ct)),
                    Status = true,
                };
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                
                return new GetByProductIdResponse()
                {
                    Data = null,
                    Status = false,
                    Error = e.Message
                };
            }
        }
    }
}