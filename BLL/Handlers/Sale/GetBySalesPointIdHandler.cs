using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Models.Sale.DTO;
using BLL.Models.Sale.GetBySalesPointId;
using Data.Intarfaces;
using MediatR;
using Microsoft.Extensions.Logging;

namespace BLL.Handlers.Sale
{
    public class GetBySalesPointIdHandler : IRequestHandler<GetBySalesPointIdRequest, GetBySalesPointIdResponse>
    {
        private readonly IMapper _mapper;
        private readonly ISaleService _service;
        private readonly ILogger<GetAllHandler> _logger;

        public GetBySalesPointIdHandler(IMapper mapper, ISaleService service, ILogger<GetAllHandler> logger)
        {
            _mapper = mapper;
            _service = service;
            _logger = logger;
        }

        public async Task<GetBySalesPointIdResponse> Handle(GetBySalesPointIdRequest request, CancellationToken ct)
        {
            try
            {
                return new GetBySalesPointIdResponse()
                {
                    Data = _mapper.Map<IList<SaleDTO>>(await _service.GetBySalesPointId(request.Id, ct)),
                    Status = true,
                };
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                
                return new GetBySalesPointIdResponse()
                {
                    Data = null,
                    Status = false,
                    Error = e.Message
                };
            }
        }
    }
}