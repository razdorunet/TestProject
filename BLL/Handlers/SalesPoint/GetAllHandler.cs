using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Models.SalesPoint.DTO;
using BLL.Models.SalesPoint.GetAll;
using Data.Intarfaces;
using MediatR;
using Microsoft.Extensions.Logging;

namespace BLL.Handlers.SalesPoint
{
    public class GetAllHandler : IRequestHandler<GetAllRequest, GetAllResponse>
    {
        private readonly IMapper _mapper;
        private readonly ISalesPointService _service;
        private readonly ILogger<GetAllHandler> _logger;

        public GetAllHandler(IMapper mapper, ISalesPointService service, ILogger<GetAllHandler> logger)
        {
            _mapper = mapper;
            _service = service;
            _logger = logger;
        }

        public async Task<GetAllResponse> Handle(GetAllRequest request, CancellationToken ct)
        {
            try
            {
                return new GetAllResponse()
                {
                    Data = _mapper.Map<IList<SalesPointDTO>>(await _service.GetAl(ct)),
                    Status = true
                };
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                
                return new GetAllResponse()
                {
                    Data = new List<SalesPointDTO>(),
                    Status = false,
                    Error = e.Message
                };
            }
        }
    }
}