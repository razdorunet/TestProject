using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Models.SalesPoint.GetById;
using BLL.Models.SalesPoint.DTO;
using Data.Intarfaces;
using MediatR;
using Microsoft.Extensions.Logging;

namespace BLL.Handlers.SalesPoint
{
    public class GetByIdHandler : IRequestHandler<GetByIdRequest, GetByIdResponse>
    {
        private readonly IMapper _mapper;
        private readonly ISalesPointService _service;
        private readonly ILogger<GetAllHandler> _logger;

        public GetByIdHandler(IMapper mapper, ISalesPointService service, ILogger<GetAllHandler> logger)
        {
            _mapper = mapper;
            _service = service;
            _logger = logger;
        }

        public async Task<GetByIdResponse> Handle(GetByIdRequest request, CancellationToken ct)
        {
            try
            {
                return new GetByIdResponse()
                {
                    Data = _mapper.Map<SalesPointDTO>(await _service.GetById(request.Id, ct)),
                    Status = true,
                };
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                
                return new GetByIdResponse()
                {
                    Data = null,
                    Status = false,
                    Error = e.Message
                };
            }
        }
    }
}