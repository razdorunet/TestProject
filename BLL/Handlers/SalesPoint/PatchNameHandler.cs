using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Models.SalesPoint.PatchName;
using Data.Intarfaces;
using MediatR;
using Microsoft.Extensions.Logging;

namespace BLL.Handlers.SalesPoint
{
    public class PatchNameHandler  : IRequestHandler<PatchNameRequest, PatchNameResponse>
    {
        private readonly IMapper _mapper;
        private readonly ISalesPointService _service;
        private readonly ILogger<PatchNameHandler> _logger;

        public PatchNameHandler(IMapper mapper, ISalesPointService service, ILogger<PatchNameHandler> logger)
        {
            _mapper = mapper;
            _service = service;
            _logger = logger;
        }
        
        public async Task<PatchNameResponse> Handle(PatchNameRequest request, CancellationToken ct)
        {
            try
            {
                return new PatchNameResponse()
                {
                    Status = await _service.PatchName(request.Id, request.Name, ct),
                };
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                
                return new PatchNameResponse()
                {
                    Status = false,
                    Error = e.Message
                };
            }
        }
    }
}