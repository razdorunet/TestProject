using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Models.SalesPoint.Post;
using Data.Intarfaces;
using MediatR;
using Microsoft.Extensions.Logging;

namespace BLL.Handlers.SalesPoint
{
    public class PostHandler  : IRequestHandler<PostSalesPointRequest, PostResponse>
    {
        private readonly IMapper _mapper;
        private readonly ISalesPointService _service;
        private readonly ILogger<PostHandler> _logger;

        public PostHandler(IMapper mapper, ISalesPointService service, ILogger<PostHandler> logger)
        {
            _mapper = mapper;
            _service = service;
            _logger = logger;
        }
        
        public async Task<PostResponse> Handle(PostSalesPointRequest salesPointRequest, CancellationToken ct)
        {
            try
            {
                var model = _mapper.Map<Data.Models.Entity.SalesPoint>(salesPointRequest.Model);
                
                return new PostResponse()
                {
                    Id = await _service.Post(model, ct),
                    Status = true,
                };
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                
                return new PostResponse()
                {
                    Id = null,
                    Status = false,
                    Error = e.Message
                };
            }
        }
    }
}