using System.Linq;
using AutoMapper;
using BLL.Models.Buyer.DTO;
using Data.Models.Entity;

namespace BLL.MappingProfiles
{
    public class BuyerHandlersMappingProfile : Profile
    {
        public BuyerHandlersMappingProfile()
        {
            CreateMap<Buyer, BuyerDTO>()
                .ForMember(d => d.SalesIds, opt => 
                    opt.MapFrom(x => x.Sales.Select(s => s.Id)));
            CreateMap<BuyerDTO, Buyer>();
            CreateMap<NewBuyerDTO, Buyer>();
        }
    }
}