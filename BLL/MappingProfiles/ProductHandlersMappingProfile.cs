using AutoMapper;
using BLL.Models.Product.DTO;
using Data.Models.Entity;

namespace BLL.MappingProfiles
{
    public class ProductHandlersMappingProfile : Profile
    {
        public ProductHandlersMappingProfile()
        {
            CreateMap<Product, ProductDTO>();
            CreateMap<ProductDTO, Product>();
            CreateMap<NewProductDTO, Product>();
        }
    }
}