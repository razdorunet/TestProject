using AutoMapper;
using BLL.Models.ProvidedProduct.DTO;
using Data.Models.Entity;

namespace BLL.MappingProfiles
{
    public class ProvidedProductHandlersMappingProfile : Profile
    {
        public ProvidedProductHandlersMappingProfile()
        {
            CreateMap<ProvidedProduct, ProvidedProductDTO>();
            CreateMap<ProvidedProductDTO, ProvidedProduct>();
            CreateMap<NewProvidedProductDTO, ProvidedProduct>();
        }
    }
}