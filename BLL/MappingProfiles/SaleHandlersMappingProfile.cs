using AutoMapper;
using BLL.Models.Sale.DTO;
using Data.Models.Entity;

namespace BLL.MappingProfiles
{
    public class SaleHandlersMappingProfile : Profile
    {
        public SaleHandlersMappingProfile()
        {
            CreateMap<Sale, SaleDTO>();
            CreateMap<SaleDTO, Sale>();
            CreateMap<NewSaleDTO, Sale>();

            CreateMap<NewSaleDataDTO, SaleData>();
            CreateMap<SaleDataDTO, SaleData>();
            CreateMap<SaleData, SaleDataDTO>();
        }
    }
}