using AutoMapper;
using BLL.Models.SalesPoint.DTO;
using Data.Models.Entity;

namespace BLL.MappingProfiles
{
    public class SalesPointHandlersMappingProfile : Profile
    {
        public SalesPointHandlersMappingProfile()
        {
            CreateMap<SalesPoint, SalesPointDTO>();
            CreateMap<SalesPointDTO, SalesPoint>();
            CreateMap<NewSalesPointDTO, SalesPoint>();
        }
    }
}