using System;

namespace BLL.Models.Base
{
    public class BaseObjectDTO
    {
        /// <summary>
        /// Is base object id
        /// </summary>
        public Guid Id { get; set; }
        
        /// <summary>
        /// Is base object name
        /// </summary>
        public string Name { get; set; }
    }
}