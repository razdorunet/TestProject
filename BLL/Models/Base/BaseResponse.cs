namespace BLL.Models.Base
{
    public class BaseResponse
    {
        /// <summary>
        /// Is response status
        /// </summary>
        public bool Status { get; set; } 
        
        /// <summary>
        /// Is response error massage
        /// </summary>
        public string Error { get; set; }
    }
}