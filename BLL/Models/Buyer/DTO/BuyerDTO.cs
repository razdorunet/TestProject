using System;
using System.Collections.Generic;
using BLL.Models.Base;

namespace BLL.Models.Buyer.DTO
{
    public class BuyerDTO : BaseObjectDTO
    {
        public IList<Guid> SalesIds { get; set; }
    }
}