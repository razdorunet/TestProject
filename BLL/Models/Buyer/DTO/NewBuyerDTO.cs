using BLL.Models.Base;

namespace BLL.Models.Buyer.DTO
{
    public class NewBuyerDTO
    {
        public string Name { get; set; }
    }
}