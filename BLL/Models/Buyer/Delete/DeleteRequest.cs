using System;
using MediatR;

namespace BLL.Models.Buyer.Delete
{
    public class DeleteRequest : IRequest<DeleteResponse>
    {
        public Guid Id { get; set; }
    }
}