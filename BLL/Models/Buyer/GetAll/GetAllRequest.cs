using MediatR;

namespace BLL.Models.Buyer.GetAll
{
    /// <summary>
    /// Is handler request all buyers
    /// </summary>
    public class GetAllRequest : IRequest<GetAllResponse>
    {
        
    }
}