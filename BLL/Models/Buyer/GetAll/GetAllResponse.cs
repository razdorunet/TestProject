using System.Collections.Generic;
using BLL.Models.Base;
using BLL.Models.Buyer.DTO;

namespace BLL.Models.Buyer.GetAll
{
    /// <summary>
    /// Is all buyers handler response
    /// </summary>
    public class GetAllResponse : BaseResponse
    {
        public IList<BuyerDTO> Data { get; set; }
    }
}