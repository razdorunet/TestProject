using System;
using MediatR;

namespace BLL.Models.Buyer.GetById
{
    public class GetByIdRequest : IRequest<GetByIdResponse>
    {
        public Guid Id { get; set; }
    }
}