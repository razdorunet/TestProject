using BLL.Models.Base;
using BLL.Models.Buyer.DTO;

namespace BLL.Models.Buyer.GetById
{
    public class GetByIdResponse : BaseResponse
    {
        public BuyerDTO Data { get; set; }
    }
}