using System;
using BLL.Models.Buyer.DTO;
using MediatR;

namespace BLL.Models.Buyer.PatchName
{
    public class PatchNameRequest : IRequest<PatchNameResponse>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}