using BLL.Models.Buyer.DTO;
using MediatR;

namespace BLL.Models.Buyer.Post
{
    public class PostBuyerRequest : IRequest<PostResponse>
    {
        public NewBuyerDTO Model { get; set; }
    }
}