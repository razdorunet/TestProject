using BLL.Models.Buyer.DTO;
using MediatR;

namespace BLL.Models.Buyer.Put
{
    public class PutBuyerRequest : IRequest<PutResponse>
    {
        public BuyerDTO Model { get; set; }
    }
}
