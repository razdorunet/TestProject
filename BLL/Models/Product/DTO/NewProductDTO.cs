using BLL.Models.Base;

namespace BLL.Models.Product.DTO
{
    public class NewProductDTO
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
    }
}