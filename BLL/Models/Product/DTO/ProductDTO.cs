using BLL.Models.Base;

namespace BLL.Models.Product.DTO
{
    public class ProductDTO : BaseObjectDTO
    {
        public decimal Price { get; set; }
    }
}