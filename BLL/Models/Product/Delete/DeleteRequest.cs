using System;
using MediatR;

namespace BLL.Models.Product.Delete
{
    public class DeleteRequest : IRequest<DeleteResponse>
    {
        public Guid Id { get; set; }
    }
}