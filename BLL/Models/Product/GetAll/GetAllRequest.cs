using MediatR;

namespace BLL.Models.Product.GetAll
{
    /// <summary>
    /// Is handler request all buyers
    /// </summary>
    public class GetAllRequest : IRequest<GetAllResponse>
    {
        
    }
}