using System.Collections.Generic;
using BLL.Models.Base;
using BLL.Models.Product.DTO;

namespace BLL.Models.Product.GetAll
{
    /// <summary>
    /// Is all buyers handler response
    /// </summary>
    public class GetAllResponse : BaseResponse
    {
        public IList<ProductDTO> Data { get; set; }
    }
}