using System;
using MediatR;

namespace BLL.Models.Product.GetById
{
    public class GetByIdRequest : IRequest<GetByIdResponse>
    {
        public Guid Id { get; set; }
    }
}