using BLL.Models.Base;
using BLL.Models.Product.DTO;

namespace BLL.Models.Product.GetById
{
    public class GetByIdResponse : BaseResponse
    {
        public ProductDTO Data { get; set; }
    }
}