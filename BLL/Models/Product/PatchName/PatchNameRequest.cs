using System;
using MediatR;

namespace BLL.Models.Product.PatchName
{
    public class PatchNameRequest : IRequest<PatchNameResponse>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}