using System;
using MediatR;

namespace BLL.Models.Product.PatchPrice
{
    public class PatchPriceRequest : IRequest<PatchPriceResponse>
    {
        public Guid Id { get; set; }
        public decimal Price { get; set; }
    }
}