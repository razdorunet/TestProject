using BLL.Models.Product.DTO;
using MediatR;

namespace BLL.Models.Product.Post
{
    public class PostProductRequest : IRequest<PostResponse>
    {
        public NewProductDTO Model { get; set; }
    }
}