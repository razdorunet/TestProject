using System;
using BLL.Models.Base;

namespace BLL.Models.Product.Post
{
    public class PostResponse : BaseResponse
    {
        public Guid? Id { get; set; }
    }
}