using BLL.Models.Buyer.DTO;
using MediatR;

namespace BLL.Models.Product.Put
{
    public class PutProductRequest : IRequest<PutResponse>
    {
        public BuyerDTO Model { get; set; }
    }
}
