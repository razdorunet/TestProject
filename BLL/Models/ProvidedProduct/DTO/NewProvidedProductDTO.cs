using System;

namespace BLL.Models.ProvidedProduct.DTO
{
    public class NewProvidedProductDTO
    {
        /// <summary>
        /// This is product quantity
        /// </summary>
        public decimal ProductQuantity { get; set; }

        /// <summary>
        /// This is product quantity unit name
        /// </summary>
        public string QuantityUnit { get; set; }
        
        /// <summary>
        /// This is product id
        /// </summary>
        public Guid ProductId { get; set; }
        
        /// <summary>
        /// This is sales point id
        /// </summary>
        public Guid SalesPointId { get; set; }
    }
}