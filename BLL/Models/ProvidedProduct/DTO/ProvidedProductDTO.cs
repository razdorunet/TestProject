using System;
using BLL.Models.Product.DTO;
using BLL.Models.SalesPoint.DTO;

namespace BLL.Models.ProvidedProduct.DTO
{
    public class ProvidedProductDTO
    {
        public decimal ProductQuantity { get; set; }

        public string QuantityUnit { get; set; }
        
        public Guid ProductId { get; set; }

        public ProductDTO Product { get; set; }

        public Guid SalesPointId { get; set; }
    }
}