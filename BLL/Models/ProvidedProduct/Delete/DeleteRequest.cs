using System;
using MediatR;

namespace BLL.Models.ProvidedProduct.Delete
{
    public class DeleteRequest : IRequest<DeleteResponse>
    {
        public Guid Id { get; set; }
    }
}