using MediatR;

namespace BLL.Models.ProvidedProduct.GetAll
{
    /// <summary>
    /// Is handler request all provided products
    /// </summary>
    public class GetAllRequest : IRequest<GetAllResponse>
    {
        
    }
}