using System.Collections.Generic;
using BLL.Models.Base;
using BLL.Models.ProvidedProduct.DTO;

namespace BLL.Models.ProvidedProduct.GetAll
{
    /// <summary>
    /// Is all buyers handler response
    /// </summary>
    public class GetAllResponse : BaseResponse
    {
        public IList<ProvidedProductDTO> Data { get; set; }
    }
}