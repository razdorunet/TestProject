using System;
using MediatR;

namespace BLL.Models.ProvidedProduct.GetById
{
    public class GetByIdRequest : IRequest<GetByIdResponse>
    {
        public Guid Id { get; set; }
    }
}