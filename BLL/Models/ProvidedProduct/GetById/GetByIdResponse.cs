using BLL.Models.Base;
using BLL.Models.ProvidedProduct.DTO;

namespace BLL.Models.ProvidedProduct.GetById
{
    public class GetByIdResponse : BaseResponse
    {
        public ProvidedProductDTO Data { get; set; }
    }
}