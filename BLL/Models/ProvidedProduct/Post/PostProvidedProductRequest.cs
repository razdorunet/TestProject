using BLL.Models.ProvidedProduct.DTO;
using MediatR;

namespace BLL.Models.ProvidedProduct.Post
{
    public class PostProvidedProductRequest : IRequest<PostResponse>
    {
        public NewProvidedProductDTO Model { get; set; }
    }
}