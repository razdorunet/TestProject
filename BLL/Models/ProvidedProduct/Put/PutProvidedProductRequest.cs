using BLL.Models.ProvidedProduct.DTO;
using MediatR;

namespace BLL.Models.ProvidedProduct.Put
{
    public class PutProvidedProductRequest : IRequest<PutResponse>
    {
        public ProvidedProductDTO Model { get; set; }
    }
}
