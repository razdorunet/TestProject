using System;
using System.Collections.Generic;

namespace BLL.Models.Sale.DTO
{
    public class NewSaleDTO
    {
        /// <summary>
        /// This is point of Sale ID
        /// </summary>
        public Guid SalesPointId { get; set; }

        /// <summary>
        /// This is buyer ID
        /// </summary>
        public Guid? BuyerId { get; set; }

        /// <summary>
        /// This is sales data collection
        /// </summary>
        public IEnumerable<NewSaleDataDTO> SalesData { get; set; }
    }
}