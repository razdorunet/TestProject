using System;

namespace BLL.Models.Sale.DTO
{
    public class NewSaleDataDTO
    {   
        /// <summary>
        /// This is sale product id
        /// </summary>
        public Guid ProductId { get; set; }

        /// <summary>
        /// This is sale product quantity
        /// </summary>
        public decimal ProductQuantity { get; set; }
        
        /// <summary>
        /// This is sale productId amount
        /// </summary>
        public decimal ProductIdAmount { get; set; }
    }
}