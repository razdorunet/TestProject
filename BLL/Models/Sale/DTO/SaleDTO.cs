using System;
using System.Collections.Generic;
using BLL.Models.Buyer.DTO;
using BLL.Models.SalesPoint.DTO;

namespace BLL.Models.Sale.DTO
{
    public class SaleDTO
    {
        public Guid Id { get; set; }
        
        public DateTimeOffset DateTime { get; set; }

        public SalesPointDTO SalesPoint { get; set; }

        public BuyerDTO Buyer { get; set; }

        public IEnumerable<SaleDataDTO> SalesData { get; set; }
    }
}