using System;
using BLL.Models.Product.DTO;

namespace BLL.Models.Sale.DTO
{
    public class SaleDataDTO
    {
        public Guid ProductId { get; set; }
        
        public ProductDTO Product { get; set; }

        public decimal ProductQuantity { get; set; }

        public decimal ProductIdAmount { get; set; }
    }
}