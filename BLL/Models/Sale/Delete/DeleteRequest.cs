using System;
using MediatR;

namespace BLL.Models.Sale.Delete
{
    public class DeleteRequest : IRequest<DeleteResponse>
    {
        public Guid Id { get; set; }
    }
}