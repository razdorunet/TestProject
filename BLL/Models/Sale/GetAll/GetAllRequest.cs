using MediatR;

namespace BLL.Models.Sale.GetAll
{
    /// <summary>
    /// Is handler request all buyers
    /// </summary>
    public class GetAllRequest : IRequest<GetAllResponse>
    {
        
    }
}