using System.Collections.Generic;
using BLL.Models.Base;
using BLL.Models.Sale.DTO;

namespace BLL.Models.Sale.GetAll
{
    /// <summary>
    /// Is all buyers handler response
    /// </summary>
    public class GetAllResponse : BaseResponse
    {
        public IList<SaleDTO> Data { get; set; }
    }
}