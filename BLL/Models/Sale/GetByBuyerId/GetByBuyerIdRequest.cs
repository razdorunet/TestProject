using System;
using MediatR;

namespace BLL.Models.Sale.GetByBuyerId
{
    public class GetByBuyerIdRequest : IRequest<GetByBuyerIdResponse>
    {
        public Guid Id { get; set; }
    }
}