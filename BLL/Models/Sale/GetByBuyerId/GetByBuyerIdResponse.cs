using System.Collections.Generic;
using BLL.Models.Base;
using BLL.Models.Sale.DTO;

namespace BLL.Models.Sale.GetByBuyerId
{
    public class GetByBuyerIdResponse : BaseResponse
    {
        public IList<SaleDTO> Data { get; set; }
    }
}