using System;
using MediatR;

namespace BLL.Models.Sale.GetById
{
    public class GetByIdRequest : IRequest<GetByIdResponse>
    {
        public Guid Id { get; set; }
    }
}