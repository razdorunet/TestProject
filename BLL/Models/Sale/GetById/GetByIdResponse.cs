using BLL.Models.Base;
using BLL.Models.Sale.DTO;

namespace BLL.Models.Sale.GetById
{
    public class GetByIdResponse : BaseResponse
    {
        public SaleDTO Data { get; set; }
    }
}