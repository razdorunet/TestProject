using System;
using MediatR;

namespace BLL.Models.Sale.GetByProductId
{
    public class GetByProductIdRequest : IRequest<GetByProductIdResponse>
    {
        public Guid Id { get; set; }
    }
}