using System.Collections.Generic;
using BLL.Models.Base;
using BLL.Models.Sale.DTO;

namespace BLL.Models.Sale.GetByProductId
{
    public class GetByProductIdResponse : BaseResponse
    {
        public IList<SaleDTO> Data { get; set; }
    }
}