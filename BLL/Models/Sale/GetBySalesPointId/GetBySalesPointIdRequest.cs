using System;
using MediatR;

namespace BLL.Models.Sale.GetBySalesPointId
{
    public class GetBySalesPointIdRequest : IRequest<GetBySalesPointIdResponse>
    {
        public Guid Id { get; set; }
    }
}