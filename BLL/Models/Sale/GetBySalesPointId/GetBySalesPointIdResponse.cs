using System.Collections.Generic;
using BLL.Models.Base;
using BLL.Models.Sale.DTO;

namespace BLL.Models.Sale.GetBySalesPointId
{
    public class GetBySalesPointIdResponse : BaseResponse
    {
        public IList<SaleDTO> Data { get; set; }
    }
}