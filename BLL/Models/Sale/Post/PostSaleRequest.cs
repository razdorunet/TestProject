using BLL.Models.Sale.DTO;
using MediatR;

namespace BLL.Models.Sale.Post
{
    public class PostSaleRequest : IRequest<PostResponse>
    {
        public NewSaleDTO Model { get; set; }
    }
}