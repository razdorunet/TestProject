using BLL.Models.Sale.DTO;
using MediatR;

namespace BLL.Models.Sale.Put
{
    public class PutSaleRequest : IRequest<PutResponse>
    {
        public SaleDTO Model { get; set; }
    }
}
