namespace BLL.Models.SalesPoint.DTO
{
    public class NewSalesPointDTO
    {
        public string Name { get; set; }
    }
}