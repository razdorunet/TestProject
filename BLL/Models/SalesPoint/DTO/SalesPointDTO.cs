using System.Collections.Generic;
using BLL.Models.Base;
using BLL.Models.ProvidedProduct.DTO;

namespace BLL.Models.SalesPoint.DTO
{
    public class SalesPointDTO : BaseObjectDTO
    {
        public IEnumerable<ProvidedProductDTO> ProvidedProducts { get; set; }
    }
}