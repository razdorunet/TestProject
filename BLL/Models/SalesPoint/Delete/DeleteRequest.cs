using System;
using MediatR;

namespace BLL.Models.SalesPoint.Delete
{
    public class DeleteRequest : IRequest<DeleteResponse>
    {
        public Guid Id { get; set; }
    }
}