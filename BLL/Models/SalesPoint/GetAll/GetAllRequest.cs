using MediatR;

namespace BLL.Models.SalesPoint.GetAll
{
    /// <summary>
    /// Is handler request all buyers
    /// </summary>
    public class GetAllRequest : IRequest<GetAllResponse>
    {
        
    }
}