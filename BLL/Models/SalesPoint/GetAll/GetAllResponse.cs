using System.Collections.Generic;
using BLL.Models.Base;
using BLL.Models.SalesPoint.DTO;

namespace BLL.Models.SalesPoint.GetAll
{
    /// <summary>
    /// Is all buyers handler response
    /// </summary>
    public class GetAllResponse : BaseResponse
    {
        public IList<SalesPointDTO> Data { get; set; }
    }
}