using System;
using MediatR;

namespace BLL.Models.SalesPoint.GetById
{
    public class GetByIdRequest : IRequest<GetByIdResponse>
    {
        public Guid Id { get; set; }
    }
}