using BLL.Models.Base;
using BLL.Models.SalesPoint.DTO;

namespace BLL.Models.SalesPoint.GetById
{
    public class GetByIdResponse : BaseResponse
    {
        public SalesPointDTO Data { get; set; }
    }
}