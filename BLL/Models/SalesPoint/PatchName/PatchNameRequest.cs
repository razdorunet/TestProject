using System;
using BLL.Models.SalesPoint.DTO;
using MediatR;

namespace BLL.Models.SalesPoint.PatchName
{
    public class PatchNameRequest : IRequest<PatchNameResponse>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}