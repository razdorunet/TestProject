using BLL.Models.SalesPoint.DTO;
using MediatR;

namespace BLL.Models.SalesPoint.Post
{
    public class PostSalesPointRequest : IRequest<PostResponse>
    {
        public NewSalesPointDTO Model { get; set; }
    }
}