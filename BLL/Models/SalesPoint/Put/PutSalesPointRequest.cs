using BLL.Models.SalesPoint.DTO;
using MediatR;

namespace BLL.Models.SalesPoint.Put
{
    public class PutSalesPointRequest : IRequest<PutResponse>
    {
        public SalesPointDTO Model { get; set; }
    }
}
