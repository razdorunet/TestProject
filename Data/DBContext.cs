using Data.Models.Entity;
using Microsoft.EntityFrameworkCore;

namespace Data
{
    /// <summary>
    /// The main structural context of the data layer database
    /// </summary>
    public class DBContext : DbContext
    {
        public DBContext(DbContextOptions<DBContext> options) :base(options)
        {
            Database.Migrate();
        }

        /// <summary>
        /// It is buyer entity
        /// </summary>
        public DbSet<Buyer> Buyers { get; set; }
        
        /// <summary>
        /// It is product entity
        /// </summary>
        public DbSet<Product> Products { get; set; }
        
        /// <summary>
        /// It is sales point entity
        /// </summary>
        public DbSet<SalesPoint> SalesPoints { get; set; }
        
        /// <summary>
        /// It is sale entity
        /// </summary>
        public DbSet<Sale> Sales { get; set; }
        
        /// <summary>
        /// It is data entity by sale entity
        /// </summary>
        public DbSet<SaleData> SalesData { get; set; }
        
        /// <summary>
        /// It is entity to entity link by sales point and product entity and his prop
        /// </summary>
        public DbSet<ProvidedProduct> ProvidedProducts { get; set; }
    }
}