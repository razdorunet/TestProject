using System;
using System.Linq;
using Data.Models.Entity;

namespace Data
{
    public static class DbInitializer
    {
        public static void Initialize(DBContext context)
        {
            context.Database.EnsureCreated();
            
            if (context.Buyers.Any())
            {
                return; 
            }

            var buyers = new Buyer[]
            {
                new Buyer { Id = Guid.Parse("cd40d7c2-c85e-4e47-891d-c64ccbe804d7"), Name = "Пупкин Ф.Н." },
                new Buyer { Id = Guid.Parse("8a15860d-1f35-44b3-a1a3-00cf16647356"), Name = "Петров Н.С." },
                new Buyer { Id = Guid.Parse("b3bd0108-ea87-4540-a39f-426222904890"), Name = "Сидоров О.Н." }
            };

            context.Buyers.AddRange(buyers);
            context.SaveChanges();
            
            
            var products = new Product[]
            {
                new Product { Id = Guid.Parse("3daa7a2d-601b-4e49-af40-cb1c179b7f5c"), Name = "Сарафан", Price = 10},
                new Product { Id = Guid.Parse("3b9f0e62-1701-4a4a-8b9c-810eafe7a7cc"), Name = "Сундук", Price = 13 }
            };

            context.Products.AddRange(products);
            context.SaveChanges();
            
            
            var salesPoints = new SalesPoint[]
            {
                new SalesPoint { Id = Guid.Parse("ceeca31e-21ae-4c4c-b9bb-f323284bd577"), Name = "У МИШИ" },
                new SalesPoint { Id = Guid.Parse("933a2c92-36b9-41a8-9f99-71ae384619b7"), Name = "Ларек под мостом" },
            };

            context.SalesPoints.AddRange(salesPoints);
            context.SaveChanges();
            
            var providedProducts = new ProvidedProduct[]
            {
                new ProvidedProduct
                {
                    Id = Guid.NewGuid(), 
                    ProductId = Guid.Parse("3daa7a2d-601b-4e49-af40-cb1c179b7f5c"), 
                    SalesPointId = Guid.Parse("ceeca31e-21ae-4c4c-b9bb-f323284bd577"),
                    ProductQuantity = 10,
                    QuantityUnit = "шт."
                },
                new ProvidedProduct
                {
                    Id = Guid.NewGuid(), 
                    ProductId = Guid.Parse("3b9f0e62-1701-4a4a-8b9c-810eafe7a7cc"), 
                    SalesPointId = Guid.Parse("ceeca31e-21ae-4c4c-b9bb-f323284bd577"),
                    ProductQuantity = 5,
                    QuantityUnit = "шт."
                },
            };

            context.ProvidedProducts.AddRange(providedProducts);
            context.SaveChanges();
        }
    }
}