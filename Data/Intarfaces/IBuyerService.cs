using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Data.Models.Entity;

namespace Data.Intarfaces
{
    public interface IBuyerService
    {
        Task<IList<Buyer>> GetAl(CancellationToken ct);
        
        Task<Buyer> GetById(Guid id, CancellationToken ct);
        
        Task<bool> Delete(Guid id, CancellationToken ct);
        
        Task<Guid> Post(Buyer model, CancellationToken ct);
        
        Task<bool> Put(Buyer model, CancellationToken ct);
        
        Task<bool> PatchName(Guid id, string name, CancellationToken ct);
    }
}