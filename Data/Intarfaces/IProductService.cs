using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Data.Models.Entity;

namespace Data.Intarfaces
{
    public interface IProductService
    {
        Task<IList<Product>> GetAll(CancellationToken ct);
        
        Task<Product> GetById(Guid id, CancellationToken ct);
        
        Task<bool> Delete(Guid id, CancellationToken ct);
        
        Task<Guid> Post(Product model, CancellationToken ct);
        
        Task<bool> Put(Product model, CancellationToken ct);
        
        Task<bool> PatchName(Guid id, string name, CancellationToken ct);
        
        Task<bool> PatchPrice(Guid id, decimal price, CancellationToken ct);
    }
}