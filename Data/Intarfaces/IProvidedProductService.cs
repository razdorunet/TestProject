using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Data.Models.Entity;

namespace Data.Intarfaces
{
    public interface IProvidedProductService
    {
        Task<IList<ProvidedProduct>> GetAl(CancellationToken ct);
        
        Task<ProvidedProduct> GetById(Guid id, CancellationToken ct);
        
        Task<bool> Delete(Guid id, CancellationToken ct);
        
        Task<Guid> Post(ProvidedProduct model, CancellationToken ct);

        Task<bool> Put(ProvidedProduct model, CancellationToken ct);
    }
}