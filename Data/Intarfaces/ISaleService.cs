using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Data.Models.Entity;

namespace Data.Intarfaces
{
    public interface ISaleService
    {
        Task<IList<Sale>> GetAl(CancellationToken ct);
        
        Task<Sale> GetById(Guid id, CancellationToken ct);

        Task<IList<Sale>> GetByProductId(Guid id, CancellationToken ct);
        
        Task<IList<Sale>> GetByBuyerId(Guid id, CancellationToken ct);
        
        Task<IList<Sale>> GetBySalesPointId(Guid id, CancellationToken ct);
        
        Task<bool> Delete(Guid id, CancellationToken ct);
        
        Task<Guid> Post(Sale model, CancellationToken ct);
        
        Task<bool> Put(Sale model, CancellationToken ct);
    }
}