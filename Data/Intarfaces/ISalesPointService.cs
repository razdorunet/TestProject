using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Data.Models.Entity;

namespace Data.Intarfaces
{
    public interface ISalesPointService
    {
        Task<IList<SalesPoint>> GetAl(CancellationToken ct);
        
        Task<SalesPoint> GetById(Guid id, CancellationToken ct);
        
        Task<bool> Delete(Guid id, CancellationToken ct);
        
        Task<Guid> Post(SalesPoint model, CancellationToken ct);
        
        Task<bool> Put(SalesPoint model, CancellationToken ct);
        
        Task<bool> PatchName(Guid id, string name, CancellationToken ct);
    }
}