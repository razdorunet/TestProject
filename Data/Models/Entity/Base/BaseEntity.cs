using System;

namespace Data.Models.Entity.Base
{
    /// <summary>
    /// Basic database table model
    /// </summary>
    public class BaseEntity
    {
        /// <summary>
        /// This is Item id in data base table
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// This is sign of a deleted entry
        /// </summary>
        public bool IsDelete { get; set; }
        
        /// <summary>
        /// This is the date and time the entry was deleted.
        /// </summary>
        public DateTimeOffset DateTimeDelete { get; set; }
    }
}