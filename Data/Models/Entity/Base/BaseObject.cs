using System.ComponentModel.DataAnnotations;

namespace Data.Models.Entity.Base
{
    /// <summary>
    /// Basic object model
    /// </summary>
    public class BaseObject : BaseEntity
    {
        /// <summary>
        /// This is object name
        /// </summary>
        [MaxLength(256)]
        public string Name { get; set; }
    }
}