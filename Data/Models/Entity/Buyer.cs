using System;
using System.Collections.Generic;
using System.Linq;
using Data.Models.Entity.Base;

namespace Data.Models.Entity
{
    /// <summary>
    /// Buyer entity model
    /// </summary>
    public class Buyer : BaseObject
    {
        public IEnumerable<Sale> Sales { get; set; }
    }
}