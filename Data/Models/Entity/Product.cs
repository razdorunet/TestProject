using Data.Models.Entity.Base;

namespace Data.Models.Entity
{
    /// <summary>
    /// Product entity model
    /// </summary>
    public class Product : BaseObject
    {
        public decimal Price { get; set; }
    }
}