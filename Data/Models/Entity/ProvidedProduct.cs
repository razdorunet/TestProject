using System;
using System.ComponentModel.DataAnnotations.Schema;
using Data.Models.Entity.Base;

namespace Data.Models.Entity
{
    /// <summary>
    /// ProvidedProduct entity model
    /// </summary>
    public class ProvidedProduct : BaseEntity
    {
        public decimal ProductQuantity { get; set; }

        public string QuantityUnit { get; set; }
        
        public Guid ProductId { get; set; }

        [ForeignKey("ProductId")]
        public Product Product { get; set; }

        public Guid SalesPointId { get; set; }
        
        [ForeignKey("SalesPointId")]
        public SalesPoint SalesPoint { get; set; }
    }
}