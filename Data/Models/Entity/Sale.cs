using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Data.Models.Entity.Base;

namespace Data.Models.Entity
{
    /// <summary>
    /// Sale entity model
    /// </summary>
    public class Sale : BaseEntity
    {
        public DateTimeOffset DateTime { get; set; }
        
        public Guid SalesPointId { get; set; }
        
        [ForeignKey("SalesPointId")]
        public SalesPoint SalesPoint { get; set; }

        public Guid? BuyerId { get; set; }

        [ForeignKey("BuyerId")]
        public Buyer Buyer { get; set; }

        public IEnumerable<SaleData> SalesData { get; set; }
    }
}