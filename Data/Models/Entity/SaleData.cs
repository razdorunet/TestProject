using System;
using System.ComponentModel.DataAnnotations.Schema;
using Data.Models.Entity.Base;

namespace Data.Models.Entity
{
    public class SaleData : BaseEntity
    {
        public Guid SaleId { get; set; }
        
        [ForeignKey("SaleId")]
        public Sale Sale { get; set; }
        
        public Guid ProductId { get; set; }

        [ForeignKey("ProductId")]
        public Product Product { get; set; }
        
        public decimal ProductQuantity { get; set; }
        
        public decimal ProductIdAmount { get; set; }
    }
}