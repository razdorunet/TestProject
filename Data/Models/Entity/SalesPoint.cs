using System.Collections.Generic;
using Data.Models.Entity.Base;

namespace Data.Models.Entity
{
    /// <summary>
    /// SalePoint entity model
    /// </summary>
    public class SalesPoint : BaseObject
    {
        public IEnumerable<ProvidedProduct> ProvidedProducts { get; set; }
    }
}