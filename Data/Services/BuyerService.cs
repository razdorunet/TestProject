using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Data.Intarfaces;
using Data.Models.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Data.Services
{
    public class BuyerService : IBuyerService
    {
        private readonly DBContext _dbContext;
        private readonly ILogger<BuyerService> _logger;

        public BuyerService(DBContext dbContext, ILogger<BuyerService> logger)
        {
            _dbContext = dbContext;
            _logger = logger;
        }
        
        public async Task<IList<Buyer>> GetAl(CancellationToken ct)
        {
            try
            {
                return await _dbContext.Buyers
                    .AsNoTracking()
                    .Where(x => !x.IsDelete)
                    .Include(x => x.Sales.Where(s => !s.IsDelete))
                    .ToListAsync(ct);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message); throw;
            }
        }

        public async Task<Buyer> GetById(Guid id, CancellationToken ct)
        {
            try
            {
                if (id == Guid.Empty)
                    throw new Exception("Id parameter cannot be empty or null");

                return await _dbContext.Buyers
                    .AsNoTracking()
                    .Where(x => !x.IsDelete)
                    .Include(x => x.Sales.Where(s => !s.IsDelete))
                    .FirstOrDefaultAsync(b => b.Id == id ,ct);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message); throw;
            }
        }

        public async Task<bool> Delete(Guid id, CancellationToken ct)
        {
            try
            {
                var model = await _dbContext.Buyers
                    .AsNoTracking()
                    .FirstOrDefaultAsync(x => x.Id == id, ct);
                
                if (model == null)
                    throw new Exception("Could not find entity item according to specified id");

                model.IsDelete = true;
                model.DateTimeDelete = DateTimeOffset.Now;
                
                _dbContext.Buyers.Update(model);
                
                await _dbContext.SaveChangesAsync(ct);

                return true;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message); throw;
            }
        }

        public async Task<Guid> Post(Buyer model, CancellationToken ct)
        {
            try
            {
                if (model == new Buyer() || model == null)
                    throw new Exception("Model parameter cannot be empty or null");

                if (string.IsNullOrEmpty(model.Name) || string.IsNullOrWhiteSpace(model.Name))
                    throw new Exception("Model.Name parameter cannot be empty or null");

                model.Id = Guid.NewGuid();
                
                await _dbContext.Buyers.AddAsync(model, ct);
                await _dbContext.SaveChangesAsync(ct);

                return model.Id;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message); throw;
            }
        }

        public async Task<bool> Put(Buyer model, CancellationToken ct)
        {
            try
            {
                if (model == new Buyer())
                    throw new Exception("Model parameter cannot be empty or null");

                if (model.Id == Guid.Empty)
                    throw new Exception("Model.Id parameter cannot be empty or null");
                
                _dbContext.Buyers.Update(model);
            
                await _dbContext.SaveChangesAsync(ct);

                return true;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message); throw;
            }
        }

        public async Task<bool> PatchName(Guid id, string name, CancellationToken ct)
        {
            try
            {
                if (id == Guid.Empty)
                    throw new Exception("Id parameter cannot be empty or null");
                
                if (string.IsNullOrEmpty(name) || string.IsNullOrWhiteSpace(name))
                    throw new Exception("Name parameter cannot be empty or null");
            
                var model = await _dbContext.Buyers.FirstOrDefaultAsync(b => b.Id == id, ct);

                model.Name = name;

                _dbContext.Buyers.Update(model);
                
                await _dbContext.SaveChangesAsync(ct);

                return true;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message); throw;
            }
        }
    }
}