using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Data.Intarfaces;
using Data.Models.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Data.Services
{
    public class ProductService : IProductService
    {
        private readonly DBContext _dbContext;
        private readonly ILogger<ProductService> _logger;

        public ProductService(DBContext dbContext, ILogger<ProductService> logger)
        {
            _dbContext = dbContext;
            _logger = logger;
        }
        
        public async Task<IList<Product>> GetAll(CancellationToken ct)
        {
            try
            {
                return await _dbContext.Products
                    .AsNoTracking()
                    .Where(x => !x.IsDelete)
                    .ToListAsync(ct);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message); throw;
            }
        }

        public async Task<Product> GetById(Guid id, CancellationToken ct)
        {
            try
            {
                if (id == Guid.Empty)
                    throw new Exception("Id parameter cannot be empty or null");

                return await _dbContext.Products
                    .AsNoTracking()
                    .Where(x => !x.IsDelete)
                    .FirstOrDefaultAsync(b => b.Id == id ,ct);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message); throw;
            }
        }
        
        public async Task<bool> Delete(Guid id, CancellationToken ct)
        {
            try
            {
                var model = await _dbContext.Products
                    .AsNoTracking()
                    .FirstOrDefaultAsync(x => x.Id == id, ct);
                
                if (model == null)
                    throw new Exception("Could not find entity item according to specified id");

                model.IsDelete = true;
                model.DateTimeDelete = DateTimeOffset.Now;
                
                _dbContext.Products.Update(model);
                
                await _dbContext.SaveChangesAsync(ct);

                return true;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message); throw;
            }
        }

        public async Task<Guid> Post(Product model, CancellationToken ct)
        {
            try
            {
                if (model == new Product() || model == null)
                    throw new Exception("Model parameter cannot be empty or null");

                if (string.IsNullOrEmpty(model.Name) || string.IsNullOrWhiteSpace(model.Name))
                    throw new Exception("Model.Name parameter cannot be empty or null");
                
                if (model.Price <= 0)
                    throw new Exception("Price parameter cannot be negative or equal to 0");
                
                model.Id = Guid.NewGuid();
                
                await _dbContext.Products.AddAsync(model, ct);
                await _dbContext.SaveChangesAsync(ct);

                return model.Id;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message); throw;
            }
        }

        public async Task<bool> Put(Product model, CancellationToken ct)
        {
            try
            {
                if (model == new Product())
                    throw new Exception("Model parameter cannot be empty or null");

                if (model.Id == Guid.Empty)
                    throw new Exception("Model.Id parameter cannot be empty or null");
                
                if (model.Price <= 0)
                    throw new Exception("Price parameter cannot be negative or equal to 0");
                
                _dbContext.Products.Update(model);
            
                await _dbContext.SaveChangesAsync(ct);

                return true;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message); throw;
            }
        }

        public async Task<bool> PatchName(Guid id, string name, CancellationToken ct)
        {
            try
            {
                if (id == Guid.Empty)
                    throw new Exception("Id parameter cannot be empty or null");
                
                if (string.IsNullOrEmpty(name) || string.IsNullOrWhiteSpace(name))
                    throw new Exception("Name parameter cannot be empty or null");
            
                var model = await _dbContext.Products.FirstOrDefaultAsync(b => b.Id == id, ct);

                model.Name = name;

                _dbContext.Products.Update(model);
                
                await _dbContext.SaveChangesAsync(ct);

                return true;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message); throw;
            }
        }

        public async Task<bool> PatchPrice(Guid id, decimal price, CancellationToken ct)
        {
            try
            {
                if (id == Guid.Empty)
                    throw new Exception("Id parameter cannot be empty or null");
                
                if (price <= 0)
                    throw new Exception("Price parameter cannot be negative or equal to 0");
            
                var model = await _dbContext.Products.FirstOrDefaultAsync(b => b.Id == id, ct);

                model.Price = price;

                _dbContext.Products.Update(model);
                
                await _dbContext.SaveChangesAsync(ct);

                return true;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message); throw;
            }
        }
    }
}