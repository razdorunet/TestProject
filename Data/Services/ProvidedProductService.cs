using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Data.Intarfaces;
using Data.Models.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Data.Services
{
    public class ProvidedProductService : IProvidedProductService
    {
        private readonly DBContext _dbContext;
        private readonly ILogger<ProvidedProductService> _logger;

        public ProvidedProductService(DBContext dbContext, ILogger<ProvidedProductService> logger)
        {
            _dbContext = dbContext;
            _logger = logger;
        }
        
        public async Task<IList<ProvidedProduct>> GetAl(CancellationToken ct)
        {
            try
            {    
                return await _dbContext.ProvidedProducts
                    .AsNoTracking()
                    .Where(x => !x.IsDelete)
                    .ToListAsync(ct);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message); throw;
            }
        }

        public async Task<ProvidedProduct> GetById(Guid id, CancellationToken ct)
        {
            try
            {
                if (id == Guid.Empty)
                    throw new Exception("Id parameter cannot be empty or null");

                return await _dbContext.ProvidedProducts
                    .AsNoTracking()
                    .Where(x => !x.IsDelete)
                    .FirstOrDefaultAsync(b => b.Id == id ,ct);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message); throw;
            }
        }

        public async Task<bool> Delete(Guid id, CancellationToken ct)
        {
            try
            {
                var model = await _dbContext.ProvidedProducts
                    .AsNoTracking()
                    .FirstOrDefaultAsync(x => x.Id == id, ct);
                
                if (model == null)
                    throw new Exception("Could not find entity item according to specified id");

                model.IsDelete = true;
                model.DateTimeDelete = DateTimeOffset.Now;
                
                _dbContext.ProvidedProducts.Update(model);
                
                await _dbContext.SaveChangesAsync(ct);

                return true;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message); throw;
            }
        }

        public async Task<Guid> Post(ProvidedProduct model, CancellationToken ct)
        {
            try
            {
                if (model == new ProvidedProduct() || model == null)
                    throw new Exception("Model parameter cannot be empty or null");

                var oldProvidedProduct = await _dbContext.ProvidedProducts
                    .AsNoTracking()
                    .Where(x => !x.IsDelete)
                    .Where(x => x.SalesPointId == model.SalesPointId)
                    .Where(x => x.ProductId == model.ProductId)
                    .FirstOrDefaultAsync(ct);

                if (oldProvidedProduct == null)
                {
                    model.Id = Guid.NewGuid();
                    await _dbContext.ProvidedProducts.AddAsync(model, ct);
                }
                else
                {
                    oldProvidedProduct.ProductQuantity += model.ProductQuantity;
                    _dbContext.ProvidedProducts.Update(oldProvidedProduct);
                }

                await _dbContext.SaveChangesAsync(ct);

                return model.Id;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message); throw;
            }
        }

        public async Task<bool> Put(ProvidedProduct model, CancellationToken ct)
        {
            try
            {
                if (model == new ProvidedProduct())
                    throw new Exception("Model parameter cannot be empty or null");

                if (model.Id == Guid.Empty)
                    throw new Exception("Model.Id parameter cannot be empty or null");
                
                _dbContext.ProvidedProducts.Update(model);
            
                await _dbContext.SaveChangesAsync(ct);

                return true;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message); throw;
            }
        }
    }
}