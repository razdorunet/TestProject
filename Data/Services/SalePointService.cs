using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Data.Intarfaces;
using Data.Models.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Data.Services
{
    public class SalesPointService : ISalesPointService
    {
        private readonly DBContext _dbContext;
        private readonly ILogger<SalesPointService> _logger;

        public SalesPointService(DBContext dbContext, ILogger<SalesPointService> logger)
        {
            _dbContext = dbContext;
            _logger = logger;
        }
        
        public async Task<IList<SalesPoint>> GetAl(CancellationToken ct)
        {
            try
            {
                return await _dbContext.SalesPoints
                    .AsNoTracking()
                    .Where(x => !x.IsDelete)
                    .Include(x => x.ProvidedProducts)
                    .ThenInclude(x => x.Product)
                    .ToListAsync(ct);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message); throw;
            }
        }

        public async Task<SalesPoint> GetById(Guid id, CancellationToken ct)
        {
            try
            {
                if (id == Guid.Empty)
                    throw new Exception("Id parameter cannot be empty or null");

                return await _dbContext.SalesPoints
                    .AsNoTracking()
                    .Where(x => !x.IsDelete)
                    .Where(x => x.Id == id)
                    .Include(x => x.ProvidedProducts)
                    .ThenInclude(x => x.Product)
                    .FirstOrDefaultAsync(ct);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message); throw;
            }
        }

        public async Task<bool> Delete(Guid id, CancellationToken ct)
        {
            try
            {
                var model = await _dbContext.SalesPoints
                    .AsNoTracking()
                    .FirstOrDefaultAsync(x => x.Id == id, ct);
                
                if (model == null)
                    throw new Exception("Could not find entity item according to specified id");

                model.IsDelete = true;
                model.DateTimeDelete = DateTimeOffset.Now;
                
                _dbContext.SalesPoints.Update(model);
                
                await _dbContext.SaveChangesAsync(ct);

                return true;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message); throw;
            }
        }

        public async Task<Guid> Post(SalesPoint model, CancellationToken ct)
        {
            try
            {
                if (model == new SalesPoint() || model == null)
                    throw new Exception("Model parameter cannot be empty or null");

                if (string.IsNullOrEmpty(model.Name) || string.IsNullOrWhiteSpace(model.Name))
                    throw new Exception("Model.Name parameter cannot be empty or null");

                model.Id = Guid.NewGuid();
                
                await _dbContext.SalesPoints.AddAsync(model, ct);
                await _dbContext.SaveChangesAsync(ct);

                return model.Id;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message); throw;
            }
        }

        public async Task<bool> Put(SalesPoint model, CancellationToken ct)
        {
            try
            {
                if (model == new SalesPoint())
                    throw new Exception("Model parameter cannot be empty or null");

                if (model.Id == Guid.Empty)
                    throw new Exception("Model.Id parameter cannot be empty or null");
                
                _dbContext.SalesPoints.Update(model);
            
                await _dbContext.SaveChangesAsync(ct);

                return true;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message); throw;
            }
        }

        public async Task<bool> PatchName(Guid id, string name, CancellationToken ct)
        {
            try
            {
                if (id == Guid.Empty)
                    throw new Exception("Id parameter cannot be empty or null");
                
                if (string.IsNullOrEmpty(name) || string.IsNullOrWhiteSpace(name))
                    throw new Exception("Name parameter cannot be empty or null");
            
                var model = await _dbContext.SalesPoints
                    .AsNoTracking()
                    .FirstOrDefaultAsync(b => b.Id == id, ct);

                model.Name = name;

                _dbContext.SalesPoints.Update(model);
                
                await _dbContext.SaveChangesAsync(ct);

                return true;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message); throw;
            }
        }
    }
}