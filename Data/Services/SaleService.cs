using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Data.Intarfaces;
using Data.Models.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Data.Services
{
    public class SaleService : ISaleService
    {
        private readonly DBContext _dbContext;
        private readonly ILogger<SaleService> _logger;

        public SaleService(DBContext dbContext, ILogger<SaleService> logger)
        {
            _dbContext = dbContext;
            _logger = logger;
        }
        
        public async Task<IList<Sale>> GetAl(CancellationToken ct)
        {
            try
            {
                return await _dbContext.Sales
                    .AsNoTracking()
                    .Where(x => !x.IsDelete)
                    .Include(x => x.Buyer)
                    .Include(x => x.SalesPoint)
                    .ThenInclude(x => x.ProvidedProducts)
                    .ThenInclude(x => x.Product)
                    .Include(x => x.SalesData)
                    .ThenInclude(x => x.Product)
                    .ToListAsync(ct);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message); throw;
            }
        }

        public async Task<Sale> GetById(Guid id, CancellationToken ct)
        {
            try
            {
                if (id == Guid.Empty)
                    throw new Exception("Id parameter cannot be empty or null");

                return await _dbContext.Sales
                    .AsNoTracking()
                    .Where(x => !x.IsDelete)
                    .Where(x => x.Id == id)
                    .Include(x => x.Buyer)
                    .Include(x => x.SalesPoint)
                    .ThenInclude(x => x.ProvidedProducts)
                    .ThenInclude(x => x.Product)
                    .Include(x => x.SalesData)
                    .ThenInclude(x => x.Product)
                    .FirstOrDefaultAsync(ct);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message); throw;
            }
        }
        
        public async Task<IList<Sale>> GetByProductId(Guid id, CancellationToken ct)
        {
            try
            {
                if (id == Guid.Empty)
                    throw new Exception("Id parameter cannot be empty or null");

                return await _dbContext.Sales
                    .AsNoTracking()
                    .Where(x => !x.IsDelete)
                    .Include(x => x.SalesData)
                    .ThenInclude(x => x.Product)
                    .Where(x => x.SalesData.Any(s => s.ProductId == id))
                    .Include(x => x.Buyer)
                    .Include(x => x.SalesPoint)
                    .ThenInclude(x => x.ProvidedProducts)
                    .ThenInclude(x => x.Product)
                    .ToListAsync(ct);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message); throw;
            }
        }
        
        public async Task<IList<Sale>> GetByBuyerId(Guid id, CancellationToken ct)
        {
            try
            {
                if (id == Guid.Empty)
                    throw new Exception("Id parameter cannot be empty or null");

                return await _dbContext.Sales
                    .AsNoTracking()
                    .Where(x => !x.IsDelete)
                    .Where(x => x.BuyerId == id)
                    .Include(x => x.Buyer)
                    .Include(x => x.SalesPoint)
                    .ThenInclude(x => x.ProvidedProducts)
                    .ThenInclude(x => x.Product)
                    .Include(x => x.SalesData)
                    .ThenInclude(x => x.Product)
                    .ToListAsync(ct);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message); throw;
            }
        }
        
        public async Task<IList<Sale>> GetBySalesPointId(Guid id, CancellationToken ct)
        {
            try
            {
                if (id == Guid.Empty)
                    throw new Exception("Id parameter cannot be empty or null");

                return await _dbContext.Sales
                    .AsNoTracking()
                    .Where(x => !x.IsDelete)
                    .Where(x => x.SalesPointId == id)
                    .Include(x => x.Buyer)
                    .Include(x => x.SalesPoint)
                    .ThenInclude(x => x.ProvidedProducts)
                    .ThenInclude(x => x.Product)
                    .Include(x => x.SalesData)
                    .ThenInclude(x => x.Product)
                    .ToListAsync(ct);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message); throw;
            }
        }

        public async Task<bool> Delete(Guid id, CancellationToken ct)
        {
            try
            {
                var model = await _dbContext.Sales
                    .AsNoTracking()
                    .FirstOrDefaultAsync(x => x.Id == id, ct);
                
                if (model == null)
                    throw new Exception("Could not find entity item according to specified id");

                model.IsDelete = true;
                model.DateTimeDelete = DateTimeOffset.Now;
                
                _dbContext.Sales.Update(model);
                
                await _dbContext.SaveChangesAsync(ct);

                return true;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message); throw;
            }
        }

        public async Task<Guid> Post(Sale model, CancellationToken ct)
        {
            try
            {
                if (model == new Sale() || model == null)
                    throw new Exception("Model parameter cannot be empty or null");
                
                if (!model.SalesData.Any())
                    throw new Exception("Model SalesData parameter cannot be empty or null");
                
                if (model.SalesData.Any(x => x.ProductQuantity <= 0))
                    throw new Exception("Model SalesData parameter ProductQuantity cannot be negative or equal to 0");
                
                if (model.SalesData.Any(x => x.ProductIdAmount <= 0))
                    throw new Exception("Model SalesData parameter ProductIdAmount cannot be negative or equal to 0");

                model.Id = Guid.NewGuid();
                model.DateTime = DateTimeOffset.Now;

                await _dbContext.AddAsync(model, ct);

                var providedProductList = new List<ProvidedProduct>();
                
                foreach (var data in model.SalesData)
                {
                    var providedProduct = await _dbContext.ProvidedProducts
                        .AsNoTracking()
                        .Where(x => x.SalesPointId == model.SalesPointId)
                        .Where(x => x.ProductId == data.ProductId)
                        .FirstOrDefaultAsync(ct);

                    if (providedProduct.ProductQuantity < data.ProductQuantity)
                        throw new Exception("Invalid item quantity.");
                        
                    providedProduct.ProductQuantity -= data.ProductQuantity;
                    
                    providedProductList.Add(providedProduct);
                }
                
                _dbContext.ProvidedProducts.UpdateRange(providedProductList);
                
                await _dbContext.SaveChangesAsync(ct);
                
                return model.Id;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message); throw;
            }
        }

        public async Task<bool> Put(Sale model, CancellationToken ct)
        {
            try
            {
                if (model == new Sale() || model == null)
                    throw new Exception("Model parameter cannot be empty or null");
                
                if (model.Id == Guid.Empty)
                    throw new Exception("Model parameter Id cannot be empty or null");
                
                if (!model.SalesData.Any())
                    throw new Exception("Model SalesData parameter cannot be empty or null");
                
                if (model.SalesData.Any(x => x.ProductQuantity <= 0))
                    throw new Exception("Model SalesData parameter ProductQuantity cannot be negative or equal to 0");
                
                if (model.SalesData.Any(x => x.ProductIdAmount <= 0))
                    throw new Exception("Model SalesData parameter ProductIdAmount cannot be negative or equal to 0");
                
                _dbContext.Update(model);
            
                var providedProductList = new List<ProvidedProduct>();
                
                foreach (var data in model.SalesData)
                {
                    var providedProduct = await _dbContext.ProvidedProducts
                        .AsNoTracking()
                        .Where(x => x.SalesPointId == model.SalesPointId)
                        .Where(x => x.ProductId == data.ProductId)
                        .FirstOrDefaultAsync(ct);

                    if (providedProduct.ProductQuantity < data.ProductQuantity)
                        throw new Exception("Invalid item quantity.");
                        
                    providedProduct.ProductQuantity -= data.ProductQuantity;
                    
                    providedProductList.Add(providedProduct);
                }
                
                _dbContext.ProvidedProducts.UpdateRange(providedProductList);
                
                await _dbContext.SaveChangesAsync(ct);

                return true;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message); throw;
            }
        }
    }
}