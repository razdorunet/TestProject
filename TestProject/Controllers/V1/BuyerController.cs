using System;
using System.Threading;
using System.Threading.Tasks;
using BLL.Models.Buyer.Delete;
using BLL.Models.Buyer.GetAll;
using BLL.Models.Buyer.GetById;
using BLL.Models.Buyer.PatchName;
using BLL.Models.Buyer.Post;
using BLL.Models.Buyer.Put;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace TestProject.Controllers.V1
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class BuyerController : ControllerBase
    {
        private readonly IMediator _mediator;
        
        public BuyerController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Getting a collection of buyers
        /// </summary>
        /// <param name="ct"></param>
        /// <returns></returns>
        [HttpGet("collection/all")]
        public async Task<IActionResult> GetAll(CancellationToken ct) => 
            Ok(await _mediator.Send(new GetAllRequest(), ct));
        
        /// <summary>
        /// Getting a buyer by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        [HttpGet("item/{id}")]
        public async Task<IActionResult> GetById(Guid id, CancellationToken ct) => 
            Ok(await _mediator.Send(new GetByIdRequest { Id = id }, ct));
        
        /// <summary>
        /// Marks record as deleted
        /// </summary>
        /// <param name="ct"></param>
        /// <returns></returns>
        [HttpDelete("item/{id}")]
        public async Task<IActionResult> Delete(Guid  id, CancellationToken ct) => 
            Ok(await _mediator.Send(new DeleteRequest { Id = id }, ct));
        
        /// <summary>
        /// Buyer name update
        /// </summary>
        /// <param name="ct"></param>
        /// <returns></returns>
        [HttpPatch("item/{id}/name/{name}")]
        public async Task<IActionResult> PutcnName(Guid id, string name, CancellationToken ct) => 
            Ok(await _mediator.Send(new PatchNameRequest {Id = id, Name = name}, ct));
        
        /// <summary>
        /// Adding a buyer
        /// </summary>
        /// <param name="ct"></param>
        /// <returns></returns>
        [HttpPost("item")]
        public async Task<IActionResult> Post(PostBuyerRequest buyerRequest, CancellationToken ct) => 
            Ok(await _mediator.Send(buyerRequest, ct));
        
        /// <summary>
        /// Update all information in the buyer model
        /// </summary>
        /// <param name="ct"></param>
        /// <returns></returns>
        [HttpPut("item")]
        public async Task<IActionResult> Put(PutBuyerRequest buyerRequest, CancellationToken ct) => 
            Ok(await _mediator.Send(buyerRequest, ct));
    }
}