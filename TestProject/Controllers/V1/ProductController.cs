using System;
using System.Threading;
using System.Threading.Tasks;
using BLL.Models.Product.Delete;
using BLL.Models.Product.GetAll;
using BLL.Models.Product.GetById;
using BLL.Models.Product.PatchName;
using BLL.Models.Product.PatchPrice;
using BLL.Models.Product.Post;
using BLL.Models.Product.Put;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace TestProject.Controllers.V1
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class ProductController : ControllerBase
    {
        private readonly IMediator _mediator;
        
        public ProductController(IMediator mediator)
        {
            _mediator = mediator;
        }
        
        /// <summary>
        /// Getting a collection of products
        /// </summary>
        /// <param name="ct"></param>
        /// <returns></returns>
        [HttpGet("collection/all")]
        public async Task<IActionResult> GetAll(CancellationToken ct) => 
            Ok(await _mediator.Send(new GetAllRequest(), ct));
        
        /// <summary>
        /// Getting a products by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        [HttpGet("item/{id}")]
        public async Task<IActionResult> GetById(Guid id, CancellationToken ct) => 
            Ok(await _mediator.Send(new GetByIdRequest { Id = id }, ct));
        
        /// <summary>
        /// Marks record as deleted
        /// </summary>
        /// <param name="ct"></param>
        /// <returns></returns>
        [HttpDelete("item/{id}")]
        public async Task<IActionResult> Delete(Guid  id, CancellationToken ct) => 
            Ok(await _mediator.Send(new DeleteRequest { Id = id }, ct));
        
        /// <summary>
        /// Products name update
        /// </summary>
        /// <param name="ct"></param>
        /// <returns></returns>
        [HttpPatch("item/{id}/name/{name}")]
        public async Task<IActionResult> PutcnName(Guid id, string name, CancellationToken ct) => 
            Ok(await _mediator.Send(new PatchNameRequest { Id = id, Name = name }, ct));

        /// <summary>
        /// Products price update
        /// </summary>
        /// <param name="ct"></param>
        /// <returns></returns>
        [HttpPatch("item/{id}/price/{price}")]
        public async Task<IActionResult> PutcnPrice(Guid id, decimal price, CancellationToken ct) => 
            Ok(await _mediator.Send(new PatchPriceRequest { Id = id, Price = price }, ct));
        
        /// <summary>
        /// Adding a products
        /// </summary>
        /// <param name="ct"></param>
        /// <returns></returns>
        [HttpPost("item")]
        public async Task<IActionResult> Post(PostProductRequest productRequest, CancellationToken ct) => 
            Ok(await _mediator.Send(productRequest, ct));
        
        /// <summary>
        /// Update all information in the products model
        /// </summary>
        /// <param name="ct"></param>
        /// <returns></returns>
        [HttpPut("item")]
        public async Task<IActionResult> Put(PutProductRequest productRequest, CancellationToken ct) => 
            Ok(await _mediator.Send(productRequest, ct));
    }
}