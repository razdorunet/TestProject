using System;
using System.Threading;
using System.Threading.Tasks;
using BLL.Models.ProvidedProduct.Delete;
using BLL.Models.ProvidedProduct.GetAll;
using BLL.Models.ProvidedProduct.GetById;
using BLL.Models.ProvidedProduct.Post;
using BLL.Models.ProvidedProduct.Put;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace TestProject.Controllers.V1
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class ProvidedProductsController : ControllerBase
    {
        private readonly IMediator _mediator;
        
        public ProvidedProductsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Getting a collection of ProvidedProduct
        /// </summary>
        /// <param name="ct"></param>
        /// <returns></returns>
        [HttpGet("collection/all")]
        public async Task<IActionResult> GetAll(CancellationToken ct) => 
            Ok(await _mediator.Send(new GetAllRequest(), ct));
        
        /// <summary>
        /// Getting a ProvidedProduct by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        [HttpGet("item/{id}")]
        public async Task<IActionResult> GetById(Guid id, CancellationToken ct) => 
            Ok(await _mediator.Send(new GetByIdRequest { Id = id }, ct));
        
        /// <summary>
        /// Marks record as deleted
        /// </summary>
        /// <param name="ct"></param>
        /// <returns></returns>
        [HttpDelete("item/{id}")]
        public async Task<IActionResult> Delete(Guid  id, CancellationToken ct) => 
            Ok(await _mediator.Send(new DeleteRequest { Id = id }, ct));

        /// <summary>
        /// Adding a ProvidedProduct
        /// </summary>
        /// <param name="ct"></param>
        /// <returns></returns>
        [HttpPost("item")]
        public async Task<IActionResult> Post(PostProvidedProductRequest providedProductRequest, CancellationToken ct) => 
            Ok(await _mediator.Send(providedProductRequest, ct));
        
        /// <summary>
        /// Update all information in the ProvidedProduct model
        /// </summary>
        /// <param name="ct"></param>
        /// <returns></returns>
        [HttpPut("item")]
        public async Task<IActionResult> Put(PutProvidedProductRequest providedProductRequest, CancellationToken ct) => 
            Ok(await _mediator.Send(providedProductRequest, ct));
    }
}