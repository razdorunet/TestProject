using System;
using System.Threading;
using System.Threading.Tasks;
using BLL.Models.Sale.Delete;
using BLL.Models.Sale.GetAll;
using BLL.Models.Sale.GetByBuyerId;
using BLL.Models.Sale.GetById;
using BLL.Models.Sale.GetByProductId;
using BLL.Models.Sale.GetBySalesPointId;
using BLL.Models.Sale.Post;
using BLL.Models.Sale.Put;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace TestProject.Controllers.V1
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class SaleController : ControllerBase
    {
        private readonly IMediator _mediator;
        
        public SaleController(IMediator mediator)
        {
            _mediator = mediator;
        }
        
        /// <summary>
        /// Getting a collection of sale
        /// </summary>
        /// <param name="ct"></param>
        /// <returns></returns>
        [HttpGet("collection/all")]
        public async Task<IActionResult> GetAll(CancellationToken ct) => 
            Ok(await _mediator.Send(new GetAllRequest(), ct));
        
        /// <summary>
        /// Getting a sales by salespoint id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        [HttpGet("collection/salespoint/{salesPointId}")]
        public async Task<IActionResult> GetBySalePointId(Guid salesPointId, CancellationToken ct) => 
            Ok(await _mediator.Send(new GetBySalesPointIdRequest { Id = salesPointId }, ct));
        
        /// <summary>
        /// Getting a sales by buyer Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        [HttpGet("collection/buyer/{buyerId}")]
        public async Task<IActionResult> GetByBuyerId(Guid buyerId, CancellationToken ct) => 
            Ok(await _mediator.Send(new GetByBuyerIdRequest { Id = buyerId }, ct));
        
        /// <summary>
        /// Getting a sales by product id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        [HttpGet("collection/product/{productId}")]
        public async Task<IActionResult> GetByProductId(Guid productId, CancellationToken ct) => 
            Ok(await _mediator.Send(new GetByProductIdRequest { Id = productId }, ct));
        
        
        /// <summary>
        /// Getting a sale by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        [HttpGet("item/{id}")]
        public async Task<IActionResult> GetById(Guid id, CancellationToken ct) => 
            Ok(await _mediator.Send(new GetByIdRequest { Id = id }, ct));
        
        /// <summary>
        /// Marks record as deleted
        /// </summary>
        /// <param name="ct"></param>
        /// <returns></returns>
        [HttpDelete("item/{id}")]
        public async Task<IActionResult> Delete(Guid  id, CancellationToken ct) => 
            Ok(await _mediator.Send(new DeleteRequest { Id = id }, ct));
        /// <summary>
        /// Adding a  sale
        /// </summary>
        /// <param name="ct"></param>
        /// <returns></returns>
        [HttpPost("item")]
        public async Task<IActionResult> Post(PostSaleRequest buyerRequest, CancellationToken ct) => 
            Ok(await _mediator.Send(buyerRequest, ct));
        
        /// <summary>
        /// Update all information in the Sale model
        /// </summary>
        /// <param name="ct"></param>
        /// <returns></returns>
        [HttpPut("item")]
        public async Task<IActionResult> Put(PutSaleRequest buyerRequest, CancellationToken ct) => 
            Ok(await _mediator.Send(buyerRequest, ct));
    }
}