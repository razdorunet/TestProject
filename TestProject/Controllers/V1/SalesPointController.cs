using System;
using System.Threading;
using System.Threading.Tasks;
using BLL.Models.SalesPoint.Delete;
using BLL.Models.SalesPoint.GetAll;
using BLL.Models.SalesPoint.GetById;
using BLL.Models.SalesPoint.PatchName;
using BLL.Models.SalesPoint.Post;
using BLL.Models.SalesPoint.Put;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace TestProject.Controllers.V1
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class SalesPointController : ControllerBase
    {
        private readonly IMediator _mediator;
        
        public SalesPointController(IMediator mediator)
        {
            _mediator = mediator;
        }
        
        /// <summary>
        /// Getting a collection of SalesPoint
        /// </summary>
        /// <param name="ct"></param>
        /// <returns></returns>
        [HttpGet("collection/all")]
        public async Task<IActionResult> GetAll(CancellationToken ct) => 
            Ok(await _mediator.Send(new GetAllRequest(), ct));
        
        /// <summary>
        /// Getting a SalesPoint by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        [HttpGet("item/{id}")]
        public async Task<IActionResult> GetById(Guid id, CancellationToken ct) => 
            Ok(await _mediator.Send(new GetByIdRequest { Id = id }, ct));
        
        /// <summary>
        /// Marks record as deleted
        /// </summary>
        /// <param name="ct"></param>
        /// <returns></returns>
        [HttpDelete("item/{id}")]
        public async Task<IActionResult> Delete(Guid  id, CancellationToken ct) => 
            Ok(await _mediator.Send(new DeleteRequest { Id = id }, ct));
        
        /// <summary>
        /// Buyer name update
        /// </summary>
        /// <param name="ct"></param>
        /// <returns></returns>
        [HttpPatch("item/{id}/name/{name}")]
        public async Task<IActionResult> PutcnName(Guid id, string name, CancellationToken ct) => 
            Ok(await _mediator.Send(new PatchNameRequest {Id = id, Name = name}, ct));
        
        /// <summary>
        /// Adding a SalesPoint
        /// </summary>
        /// <param name="ct"></param>
        /// <returns></returns>
        [HttpPost("item")]
        public async Task<IActionResult> Post(PostSalesPointRequest salesPointRequest, CancellationToken ct) => 
            Ok(await _mediator.Send(salesPointRequest, ct));
        
        /// <summary>
        /// Update all information in the SalesPoint model
        /// </summary>
        /// <param name="ct"></param>
        /// <returns></returns>
        [HttpPut("item")]
        public async Task<IActionResult> Put(PutSalesPointRequest salesPointRequest, CancellationToken ct) => 
            Ok(await _mediator.Send(salesPointRequest, ct));
    }
}