using Data;
using Data.Intarfaces;
using Data.Services;
using MediatR;
using BLL.MappingProfiles;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace TestProject
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAutoMapper(typeof(BuyerHandlersMappingProfile));
            services.AddAutoMapper(typeof(ProductHandlersMappingProfile));
            services.AddAutoMapper(typeof(ProvidedProductHandlersMappingProfile));
            services.AddAutoMapper(typeof(SalesPointHandlersMappingProfile));
            services.AddAutoMapper(typeof(SaleHandlersMappingProfile));

            services.AddScoped<IBuyerService, BuyerService>();
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<IProvidedProductService, ProvidedProductService>();
            services.AddScoped<ISalesPointService, SalesPointService>();
            services.AddScoped<ISaleService, SaleService>();

            services.AddDbContext<DBContext>(option =>
                option.UseSqlite(Configuration.GetConnectionString("DefaultConnection"), 
                        x => x.MigrationsAssembly("Data"))
                    .LogTo(System.Console.WriteLine));
            
            services.AddControllers();
            services.AddMediatR();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "TestProject", Version = "v1" });
            });
        }
        
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "TestProject v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}